const utils = require("../lib/internal/utils");

describe("fedrampUrlChecks", () => {
  it("should indicate a FedRAMP staging host when baseURL ends with 'atlassian-stg-fedm.net", () => {
    expect(
      utils.isFedRAMPStagingHost("http://foobar.atlassian-stg-fedm.net")
    ).toBe(true);
  });

  it("should indicate a Production FedRAMP host when baseURL ends with 'atlassian-us-gov-mod.net'", () => {
    expect(
      utils.isFedRAMPProductionHost("http://foobar.atlassian-us-gov-mod.net")
    ).toBe(true);
  });

  it("should indicate a Production FedRAMP host when baseURL has greater than 2 dots and ends with 'atlassian-us-gov-mod.net'", () => {
    expect(
      utils.isFedRAMPProductionHost("http://foo.bar.atlassian-us-gov-mod.net")
    ).toBe(true);
  });

  it("should NOT indicate FedRAMP production when baseURL doesn't end with '.atlassian-us-gov-mod.net'", () => {
    expect(utils.isFedRAMPProductionHost("http://foobar.atlassian.net")).toBe(
      false
    );
  });

  it("should indicate a commercial production host when baseURL end with '.atlassian.net'", () => {
    expect(utils.isProductionHost("http://foobar.atlassian.net")).toBe(true);
  });
});
