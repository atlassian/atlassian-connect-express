const nock = require("nock");
const helper = require("./test_helper");
const request = require("request");
const express = require("express");
const bodyParser = require("body-parser");
const ac = require("../index");
const logger = require("./logger");
const http = require("http");
const jose = require("jose");
const _ = require("lodash");
const extend = require("extend");

exports.mockInstallKeyServer = function mockInstallKeyServer() {
  nock("https://connect-install-keys.atlassian.com")
    .persist()
    .get(`/${helper.keyId}`)
    .reply(200, helper.publicKey);
};

exports.forgeInvocationSampleData = {
  app: {
    id: "ari:cloud:ecosystem::app/8db33809-1f32-48bb-8c52-5877dab48107",
    version: "16",
    installationId:
      "ari:cloud:ecosystem::installation/0a3a7799-53ae-4a5b-9e7e-03338980abb5",
    apiBaseUrl:
      "https://api.stg.atlassian.com/ex/confluence/d0d52620-3203-4cfa-8db5-f2587155f0dd",
    environment: {
      type: "DEVELOPMENT",
      id: "ari:cloud:ecosystem::environment/8db33809-1f32-48bb-8c52-5877dab48107/aa911f10-c54b-4b93-9e27-dd2947840b9e"
    },
    module: {
      type: "xen:macro",
      key: "forge-remote-app-boot"
    }
  },
  context: {
    localId: "4654fa12-4c7c-4792-95a9-6019edb27953",
    cloudId: "d0d52620-3203-4cfa-8db5-f2587155f0dd",
    moduleKey: "forge-remote-app-boot",
    siteUrl: "https://pbray2.jira-dev.com",
    extension: {
      type: "macro",
      content: {
        id: "5341185"
      },
      space: {
        key: "~655362312d3308895442b0aa38771a10c88656",
        id: "65538"
      },
      isEditing: false,
      references: []
    }
  },
  principal: "655362:312d3308-8954-42b0-aa38-771a10c88656",
  aud: "ari:cloud:ecosystem::app/8db33809-1f32-48bb-8c52-5877dab48107",
  iss: "forge/invocation-token",
  iat: 1700175149,
  nbf: 1700175149,
  exp: 1700175174,
  jti: "d8a496253ec8c18a54631e4c82cbedd5d0ae8570"
};

const generateJWKandJWTForTest = async (jwtOverrides = {}) => {
  const { publicKey, privateKey } = await jose.generateKeyPair("RS256");
  const publicJwk = await jose.exportJWK(publicKey);
  const jwt = await new jose.SignJWT(
    _.omitBy(
      {
        app: {
          id: exports.forgeInvocationSampleData.app.id,
          apiBaseUrl: exports.forgeInvocationSampleData.app.apiBaseUrl,
          installationId: jwtOverrides.app.installationId
        },
        context: {
          cloudId: exports.forgeInvocationSampleData.context.cloudId,
          siteUrl: exports.forgeInvocationSampleData.context.siteUrl
        },
        principal: exports.forgeInvocationSampleData.principal
      },
      _.isNil
    )
  )
    .setProtectedHeader({ alg: "RS256", kid: "testKeyId" })
    .setIssuedAt()
    .setIssuer("forge/invocation-token")
    .setAudience(exports.forgeInvocationSampleData.app.id)
    .setExpirationTime("1h")
    .sign(privateKey);

  return { jwks: { keys: [{ ...publicJwk, kid: "testKeyId" }] }, jwt };
};

exports.mockForgeJwksServer = async function mockForgeJwksServer(
  jwtOverrides = {}
) {
  const { jwks, jwt } = await generateJWKandJWTForTest(jwtOverrides);
  nock("https://forge.cdn.stg.atlassian-dev.net/.well-known/jwks.json")
    .get("")
    .reply(200, jwks);
  return jwt;
};

exports.installDummyTenant = function installDummyTenant(forgeInstallationId) {
  return new Promise((resolve, reject) => {
    request(
      {
        url: `${helper.addonBaseUrl}/installed`,
        method: "POST",
        json: Object.assign(
          {},
          forgeInstallationId
            ? extend(_.cloneDeep(helper.installedPayload), {
                installationId: forgeInstallationId
              })
            : helper.installedPayload
        ),
        headers: {
          Authorization: `JWT ${helper.createJwtTokenForInstall({
            method: "POST",
            path: "/installed"
          })}`
        }
      },
      (err, res) => {
        if (res.statusCode !== 204) {
          reject(
            new Error(`Install hook failed with statusCode ${res.statusCode}`)
          );
        }
        resolve();
      }
    );
  });
};

exports.startConnectServer = async function startConnectServer({
  storeKey,
  port,
  localBaseUrl,
  appId
} = {}) {
  const app = express();
  app.use(bodyParser.json());
  app.set("env", "test");
  const configuredStoreKey = storeKey || "e2e";
  ac.store.register(configuredStoreKey, (logger, opts) => {
    return require("../lib/store/sequelize")(logger, opts);
  });
  let addon;
  await new Promise(resolve => {
    addon = ac(
      app,
      {
        config: {
          appId,
          test: {
            store: {
              adapter: configuredStoreKey,
              type: "memory",
              logging: false
            },
            localBaseUrl
          }
        }
      },
      logger,
      resolve
    );
  });
  app.use(addon.middleware());
  let server;
  await new Promise(resolve => {
    server = http.createServer(app).listen(port, resolve);
  });
  return { addon, app, server };
};
