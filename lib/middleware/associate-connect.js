const requestHandler = require("./request");

module.exports = function (addon) {
  const associateConnectHandler = async function (req, res, next) {
    const forgeContext = req.context.forge;
    if (forgeContext) {
      const forgeInstallationId = forgeContext.app.installationId;
      const userAccountId = forgeContext.principal;
      const clientSetting =
        await addon.settings.getClientSettingsForForgeInstallation(
          forgeInstallationId
        );

      if (clientSetting) {
        const params = {
          clientKey: clientSetting.clientKey,
          hostBaseUrl: clientSetting.baseUrl,
          addonKey: clientSetting.key,
          key: clientSetting.key,
          userAccountId
        };

        const reqHandler = requestHandler(addon, params);
        reqHandler(req, res, next);
      } else {
        console.info(
          "No associated Connect settings found for forge installation ID",
          forgeInstallationId
        );
        next();
      }
    } else {
      console.info(
        "Bypassing associateConnectHandler as there is no forge context to be associated with."
      );
      next();
    }
  };

  return associateConnectHandler;
};
