function getAsObject(val) {
  if (typeof val === "string") {
    try {
      val = JSON.parse(val);
    } catch (e) {
      // it's OK if we can't parse this. We'll just return the string below.
    }
  }

  return val;
}

function getAsString(val) {
  if (typeof val === "string") {
    return val;
  }

  return JSON.stringify(val);
}

module.exports = {
  getAsObject,
  getAsString
};
