/* Abstraction over the Forge KV store for App token caching and retrieval */

class ForgeAppTokenCache {
  constructor(addon) {
    this.addon = addon;
  }

  /**
   * Set the Forge app token for a given installation
   *
   * @typedef {Object} ForgeAppToken
   * @property {string} appToken - The app token
   * @property {string} apiBaseUrl - The base URL of the Forge API
   *
   * @param {string} installationId - The installation ID
   * @param {string} appToken - The app token to cache
   * @param {string} apiBaseUrl - The base URL of the Forge API
   *
   * @returns {Promise<ForgeAppToken>} - The app token and API base URL
   */

  async setForgeAppToken(installationId, appToken, apiBaseUrl) {
    await this.addon.settings
      .forForgeInstallation(installationId)
      .set("appToken", { appToken, apiBaseUrl });
  }

  /**
   * Get the Forge app token for a given installation
   *
   * @param {string} installationId - The installation ID
   *
   * @returns {Promise<ForgeAppToken>} - The app token
   */

  async getForgeAppToken(installationId) {
    return this.addon.settings
      .forForgeInstallation(installationId)
      .get("appToken");
  }
}

module.exports = ForgeAppTokenCache;
