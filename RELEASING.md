# Release process

( Slack: [#help-connect](https://atlassian.slack.com/archives/CFGTZ99TL) )

1. Create a new release branch from master 
    ```
      > git checkout master
      > git pull
      > git checkout -b release/x.x.x
    ```
2. Update [release notes](./RELEASENOTES.md) 

3. Update the version by running `npm version` command with appropriate versioning semantic. 

    ```
      npm version (major|minor|patch|prerelease)
    ```
    This will simply bump the `version` in the package.json file and commit the changes.

4. Create a new PR to merge the changes from the release branch to `master` so that the `package.json` version is canonically updated. 

5. When the PR is merged, the updated package will be automatically published to the public NPM registry.
